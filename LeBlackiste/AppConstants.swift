//
//  AppConstants.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 11/9/20.
//  Copyright © 2020 TechNoire. All rights reserved.
//

import Foundation

enum CellIds: String {
    case barCell = "barCell"
    case piechartCell = "piechartCell"
}

enum StoryBoards: String {
    case main = "Main"
}

class AppConstants {
    static var Audience = ["Homme", "Femme", "Les deux"]
    static var defaultNumber = "100"
    
}

class Images {
    static var forwardArrow = "Right"
    static var checkmark = "checkmark"

}
