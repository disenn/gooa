//
//  ImageStore.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 4/9/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit


class ImageStore {
    
    static var shared = ImageStore()
    static let cache = NSCache<NSString, UIImage>()
    
    
    func setImage(key: String, image: UIImage) {
        print("SAVED key \(key)....-> \(image.description)")
        ImageStore.cache.setObject(image, forKey: key as NSString)
        print("so now if we retrieve it it's \(ImageStore.cache.object(forKey: key as NSString)?.description)")
    }
    
    func getImage(key: String) -> UIImage? {
        print("getting image for key.... \(key) \(ImageStore.cache.object(forKey: key as NSString)?.description)")
        return ImageStore.cache.object(forKey: key as NSString)
    }
    
    func removeImage(key: String) {
        ImageStore.cache.removeObject(forKey: key as NSString)
    }
    
    func removeAllImages() {
        ImageStore.cache.removeAllObjects()
    }
}
