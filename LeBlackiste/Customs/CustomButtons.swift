//
//  CustomButtons.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/8/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable

class RoundButton: UIButton {

// MARK: - Properties
@IBInspectable var checked: Bool = false {
    didSet {
        // Toggle the check/uncheck images
        corner()
    }
}


override init(frame: CGRect) {
    super.init(frame: frame)
    //setup()
}

required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    //setup()
}

override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    //setup()
}

internal func setup() {
    //self.addTarget(self, action: #selector(tapped), for: .touchUpInside)
}

private func corner() {
    self.layer.cornerRadius = self.frame.height / 2
}

/// Called each time the button is tapped, and toggles the checked property
@objc private func tapped() {
    checked = !checked
    print("New value: \(checked)")
}
}

@IBDesignable

class CircleImage: UIImageView {

// MARK: - Properties
@IBInspectable var checked: Bool = false {
    didSet {
        // Toggle the check/uncheck images
        corner()
    }
}


override init(frame: CGRect) {
    super.init(frame: frame)
    //setup()
}

required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    //setup()
}

override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    //setup()
}

internal func setup() {
    //self.addTarget(self, action: #selector(tapped), for: .touchUpInside)
}

private func corner() {
    self.layer.borderWidth = 3.0
    self.layer.masksToBounds = false
    self.layer.borderColor = UIColor.blackTheme.cgColor
    self.layer.cornerRadius = self.frame.size.width / 2
    self.clipsToBounds = true
}

/// Called each time the button is tapped, and toggles the checked property
@objc private func tapped() {
    checked = !checked
}
}

