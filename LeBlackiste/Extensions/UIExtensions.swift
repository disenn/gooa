//
//  UIExtensions.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/8/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit

enum NotificationType {
    case subscribe
    case post
}
extension UIViewController {
    
    func createVC(id: String, storyboard: StoryBoards) -> UIViewController {
        let vc = UIStoryboard(name: storyboard.rawValue, bundle: nil).instantiateViewController(withIdentifier: id)
        vc.modalPresentationStyle = .fullScreen
        return vc
    }
    
    func createVC(id: String, storyboard: StoryBoards, style: UIModalPresentationStyle) -> UIViewController {
        let vc = UIStoryboard(name: storyboard.rawValue, bundle: nil).instantiateViewController(withIdentifier: id)
        vc.modalPresentationStyle = style
        return vc
    }
    
//    func createPopover() {
//
//        var popoverContent = self.storyboard?.instantiateViewControllerWithIdentifier("NewCategory") as UIViewController
//        var nav = UINavigationController(rootViewController: popoverContent)
//        nav.modalPresentationStyle = UIModalPresentationStyle.Popover
//        var popover = nav.popoverPresentationController
//        popoverContent.preferredContentSize = CGSizeMake(500,600)
//        popover.delegate = self
//        popover.sourceView = self.view
//        popover.sourceRect = CGRectMake(100,100,0,0)
//
//        self.presentViewController(nav, animated: true, completion: nil)
//
//    }
    
    func addGradient() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.startPoint  = CGPoint(x: 0, y: 0)
        
        gradientLayer.colors = [UIColor.grayTheme.cgColor,
                                UIColor.white.cgColor]
        
        gradientLayer.transform = CATransform3DMakeRotation(CGFloat.pi / 2, 0, 0, 1)
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.layer.addSublayer(gradientLayer)
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func subbus(action: Selector) {
        NotificationCenter.default.addObserver(
            self,
            selector: action,
            name: .none,
            object: nil
        )
    }
    
    func displayPrompt(message: String) {
        let vc = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        vc.addAction(action)
        self.present(vc, animated: true, completion: nil)
    }
    
    func post(name: NSNotification.Name, infos: [AnyHashable : Any]) {
        NotificationCenter.default.post(name: name, object: nil, userInfo: infos)
    }
    
    func register(name: NSNotification.Name, selector: Selector) {
        NotificationCenter.default.addObserver(self, selector: selector, name: name, object: nil)
    }
    
//    func subscribeToKeyBoard() {
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(keyboardWillShow),
//            name: UIResponder.keyboardWillShowNotification,
//            object: nil
//        )
//    }
//    
//    @objc func keyboardWillShow(_ notification: Notification) {
//        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
//            let keyboardRectangle = keyboardFrame.cgRectValue
//            let keyboardHeight = keyboardRectangle.height
//            let ypos = keyboardRectangle.origin.y
//            print("Keyboard height is \(keyboardRectangle.height)")
//            print("Keyboard position is \(ypos)")
//        }
//    }
    
    func createCell(nibName: String) -> UINib {
        let cell = UINib(nibName: nibName, bundle: nil)
        return cell
    }
    
    func imagePikerController(source: UIImagePickerController.SourceType, key: String) -> UIImagePickerController {
        let imagePiker = UIImagePickerController()
        imagePiker.sourceType = source
        imagePiker.title = key
         return imagePiker
    }
    
    func addGradient(firstColor: UIColor, secondColor: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.startPoint  = CGPoint(x: 0, y: 0)
        
        gradientLayer.colors = [firstColor.cgColor,
                                secondColor.cgColor]
        
        gradientLayer.transform = CATransform3DMakeRotation(CGFloat.pi / 2, 0, 0, 1)
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.width / 2, height: self.view.frame.height)
        self.view.layer.addSublayer(gradientLayer)
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        
    }
}

extension UIColor {
    convenience init(hex: String, alpha: CGFloat = 1.0) {
        let hexString: String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func hex() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
    
    static var yellowTheme = UIColor(hex: "#DAE025")
    static var grayTheme = UIColor(hex: "#A6A6A6")
    static var blackTheme = UIColor(hex: "#000000")
    static var gold = UIColor(hex: "#ffe700ff")
}


extension UIButton {
    func roundButton() {
        self.layer.cornerRadius = self.frame.width / 2
        //self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
}

extension UITableView {
    func makeFooter() {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        view.backgroundColor = .clear
        self.tableFooterView = view
    }
}


extension UITableViewCell {
    
    func getID() -> String {
        return self.reuseIdentifier ?? "not set"
    }
}

extension UITextField {
    func themeBorderDisplay() {
        self.layer.borderWidth = 2.0
        self.layer.borderColor = UIColor.blackTheme.cgColor
    }
}

extension NSNotification.Name {
    
    static let BasicShippingInfo = NSNotification.Name(rawValue: "BasicShippingInfo")
    static let CategoryInfo = NSNotification.Name(rawValue: "CategoryInfo")
}
