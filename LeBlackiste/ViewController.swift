//
//  ViewController.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 10/9/20.
//  Copyright © 2020 TechNoire. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    static func createMainVC() -> UIViewController {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MainVC")
        return vc
    }
    
    @IBAction func gotoCreateShop(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "createshopVC")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }

    
    func setupUI() {
        self.addGradient()
    }

}

