//
//  MenuViewController.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/21/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import UIKit

protocol MenuActions {
    func gotoCustomizeStore()
    func gotoAddItems()
    func gotoStoreProfile()
    func gotoInsights()
    func gotoTermsAndConditions()
    func gotoQASession()
    func gotoOrders()
    func gotoAbout()
    func gotoSubscriptions()
}



class MenuViewController: UIViewController {
    
    
    func performActions(actionNumber: Int) {
        switch actionNumber {
        case 0:
            self.menuAction?.gotoInsights()
        case 1:
            self.menuAction?.gotoAddItems()
        case 2:
            self.menuAction?.gotoOrders()
        case 3:
            self.menuAction?.gotoOrders()
        case 4:
            self.menuAction?.gotoAbout()
        case 5:
            self.menuAction?.gotoTermsAndConditions()
        case 6:
            self.menuAction?.gotoStoreProfile()
        case 7:
            self.menuAction?.gotoSubscriptions()
        default:
            self.menuAction?.gotoInsights()
        }
    }
    
    var menuAction: MenuActions?

    @IBOutlet weak var tableView: UITableView!
    var menuItems = ["Insights","Add items","Premium features","Orders","About","QA session","Terms And Conditions","Store profile","Subscriptions"]
    
    var menuItemsImages = ["book-black-minimalistic","book-black-minimalistic","book-black-minimalistic","lock-black-minimalist","lock-black-minimalist","medical-black-minimalist","medical-black-minimalist","medical-black-minimalist","medical-black-minimalist"]
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        self.tableView.backgroundColor = .clear
        self.addGradient(firstColor: .white, secondColor: .lightGray)
        tableView.register(createCell(nibName: "NameAndLogoCell"), forCellReuseIdentifier: "namelogoCell")

        tableView.delegate = self
        tableView.dataSource = self
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        view.backgroundColor = .clear
        tableView.tableFooterView = view
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "namelogoCell", for: indexPath) as! NameAndLogoCell
        cell.displayCell(title: menuItems[indexPath.row], image: menuItemsImages[indexPath.row], action: {
            self.performActions(actionNumber: indexPath.row)
        })
        return cell
    }
    
    
    
}
