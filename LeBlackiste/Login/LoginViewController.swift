//
//  LoginViewController.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/8/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    static func create() -> UIViewController {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "LoginVC")
        return vc
    }
    
    @IBAction func tapDetails(_ sender: Any) {
        let vc = ViewController.createMainVC()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()

        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        self.addGradient()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
