//
//  CreateBoutiqueViewController.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 11/9/20.
//  Copyright © 2020 TechNoire. All rights reserved.
//

import UIKit

class CreateBoutiqueViewController: UIViewController {
    
    @IBOutlet weak var myTableView: UITableView!
    var listOfCells = [UINib]()
    var logoImage = UIImage()
    var imagePick = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addGradient()
        let nameCell = UINib(nibName: "NameLabelCell", bundle: nil)
        let nicknameCell = UINib(nibName: "NameLabelCell", bundle: nil)
        let phoneCell = UINib(nibName: "NameLabelCell", bundle: nil)
        let emailCell = UINib(nibName: "NameLabelCell", bundle: nil)
        let pickerCell = UINib(nibName: "PickerCell", bundle: nil)
        let pictureCell = UINib(nibName: "PictureCell", bundle: nil)
        let singleButtonCell = UINib(nibName: "SingleButton", bundle: nil)
        let categoryButtonsCell = UINib(nibName: "CategoriesPickerCell", bundle: nil)
        listOfCells.append(nameCell)
        listOfCells.append(nicknameCell)
        listOfCells.append(phoneCell)
        listOfCells.append(emailCell)
        listOfCells.append(pickerCell)
        listOfCells.append(pictureCell)
        listOfCells.append(singleButtonCell)
        listOfCells.append(categoryButtonsCell)
        myTableView.register(nameCell, forCellReuseIdentifier: "namelabelCell")
        myTableView.register(pickerCell, forCellReuseIdentifier: "pickerviewCell")
        myTableView.register(pictureCell, forCellReuseIdentifier: "pictureCell")
        myTableView.register(singleButtonCell, forCellReuseIdentifier: "singlebuttonCell")
        myTableView.register(categoryButtonsCell, forCellReuseIdentifier: "categoriespickerCell")
        //categoriespickerCell
        myTableView.delegate = self
        myTableView.dataSource = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(resignKeyBoard))
        self.view.addGestureRecognizer(tap)
        imagePick.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        testVC()
    }
    
    func testVC() {
        //managerVC
//        let     vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addVC")
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "managerVC")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func resignKeyBoard() {
        self.view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CreateBoutiqueViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "namelabelCell", for: indexPath) as! NameLabelCell
            cell.display(name: "Nom de la boutique*", keyboardType: .default)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "namelabelCell", for: indexPath) as! NameLabelCell
            cell.display(name: "Nickname", keyboardType: .default)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "namelabelCell", for: indexPath) as! NameLabelCell
            cell.display(name: "Numéro de telephone*", keyboardType: .phonePad)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "namelabelCell", for: indexPath) as! NameLabelCell
            cell.display(name: "Email*", keyboardType: .emailAddress)
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "pickerviewCell", for: indexPath) as! PickerCell
            cell.display(title: "Clientèle", items: AppConstants.Audience)
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "pictureCell", for: indexPath) as! PictureCell
            cell.pictureView = self
            cell.display(image: self.logoImage)
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoriespickerCell", for: indexPath) as! CategoriesPickerCell
            cell.display(categoryLimit: 3) { (categories) in
                //
            }
            return cell
        case 7:
            let cell = tableView.dequeueReusableCell(withIdentifier: "singlebuttonCell", for: indexPath) as! SingleButton
            cell.delegate = self
            cell.display(name: "Créer Boutique", active: false)
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "namelabelCell", for: indexPath) as! NameLabelCell
            cell.display(name: "Nom de la boutique*", keyboardType: .default)
            return cell
        }
    }
    
    
}
extension CreateBoutiqueViewController: PictureView {
    func imagePicker() {
        imagePick.delegate = self
        imagePick.sourceType = .savedPhotosAlbum
        imagePick.allowsEditing = false
        present(imagePick, animated: true, completion: nil)
    }
    
    
}
extension CreateBoutiqueViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: nil)
        if let possibleImage = info[.editedImage] as? UIImage {
            self.logoImage = possibleImage
            self.myTableView.reloadData()
            } else if let possibleImage = info[.originalImage] as? UIImage {
                self.logoImage = possibleImage
                self.myTableView.reloadData()
            } else {
                return
            }
    }
}
extension CreateBoutiqueViewController: SingleActionView {
    func action() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
