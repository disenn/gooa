//
//  Product.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 4/15/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import RealmSwift

class ProductImages: Object {
    @objc dynamic var mainImage: Data?
    @objc dynamic var leftImage: Data?
    @objc dynamic var middleImage: Data?
    @objc dynamic var rightImage: Data?
}

class Variantx: Object {
    @objc dynamic var name = ""
    var variants = List<String>()
    @objc dynamic var placeHolder = ""
    @objc dynamic var stock = ""
}

class Product: Object {
    let localId = UUID().uuidString
    @objc dynamic var images: ProductImages?
    @objc dynamic var name = ""
    @objc dynamic var desc = ""
    @objc dynamic var price = ""
    @objc dynamic var stock = ""
    @objc dynamic var shippingPrice = ""
    @objc dynamic var estimation = ""
    @objc dynamic var categorie = ""
    @objc dynamic var selectedVariant = ""
    @objc dynamic var variants: Variantx?
    
}
