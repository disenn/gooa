//
//  LaunchViewController.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/8/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import UIKit
import Lottie

class LaunchViewController: UIViewController {
    
    @IBOutlet weak var animationView: AnimationView!
    
    @IBAction func tapStart(_ sender: Any) {
        let vc = LoginViewController.create()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addGradient()
        animate()
        // Do any additional setup after loading the view.
    }
    
    private func animate() {
        //3.0.4
        self.animationView.animation = Animation.named("vstore")
        animationView.loopMode = .loop
        animationView.play { (play) in
            if !play {
                self.animationView.forceDisplayUpdate()
                self.animationView.play()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        animationView.play()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
