//
//  AnyViewController.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 4/6/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import UIKit


enum SupportedVC {
    case ShippingVC
    case CategoriesVC
}
class AnyViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var presenter: AnyViewControllerPresenter?
    
    @IBAction func backAction(_ sender: Any) {
        self.post(name: .BasicShippingInfo, infos: ["infos": BasicShippingInfo(price: "25000", estimate: "24 Heures")])
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func saveAction(_ sender: Any) {
        self.presenter?.save(vc: supportedVC)
    }
    
    var supportedVC: SupportedVC!
    
    static func create(for vc: SupportedVC) -> UIViewController {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AnyVC") as! AnyViewController
        viewController.supportedVC = vc
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCells()
        setupUI()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
        presenter = AnyViewControllerPresenter(view: self)
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        switch supportedVC {
        case .CategoriesVC:
            titleLabel.text = "Choisissez votre catégorie.\n"

//            titleLabel.text = "Choisissez minimum une catégorie. \n Les clients pourront trouver cet article sous la/les catégorie(s)\n que vous choisisserai."
        case .ShippingVC:
            titleLabel.text = "Livraison à Abidjan. \n "
        default:
            titleLabel.text = "\n "
        }
        
    }
    
    func setupCells() {
        switch supportedVC {
        case .CategoriesVC:
            let categoryButtonsCell = UINib(nibName: "CategoriesPickerCell", bundle: nil)
            tableView.register(categoryButtonsCell, forCellReuseIdentifier: CategoriesPickerCell.getID)

        case .ShippingVC:
            let shippingCell = UINib(nibName: "ShippingCell", bundle: nil)
            tableView.register(shippingCell, forCellReuseIdentifier: ShippingCell.getID)
        default:
            let shippingCell = UINib(nibName: "ShippingCell", bundle: nil)
            tableView.register(shippingCell, forCellReuseIdentifier: ShippingCell.getID)
        }
        
        tableView.delegate = self
        tableView.dataSource = self
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}
extension AnyViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        switch supportedVC {
        case .CategoriesVC:
            let cell = tableView.dequeueReusableCell(withIdentifier: CategoriesPickerCell.getID, for: indexPath) as! CategoriesPickerCell
            cell.display(categoryLimit: 1) { (categories) in
                self.presenter?.savecategory(categories: categories)
            }
            return cell
            
        case .ShippingVC:
            let cell = tableView.dequeueReusableCell(withIdentifier: ShippingCell.getID, for: indexPath) as! ShippingCell
            cell.displayCell { (price) in
                self.presenter?.saveShippingPrice(price: price)
            } actionEstimate: { (estimate) in
                self.presenter?.saveEstimate(estimate: estimate)
            }

            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: ShippingCell.getID, for: indexPath) as! ShippingCell
            //cell.displayCell()
        }
        return cell
    }
        
}

extension AnyViewController: AnyVCView {
    func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showPrompt(prompt: String) {
        self.displayPrompt(message: prompt)
    }
    
    func saveShipping(shippingInfo: BasicShippingInfo) {
        self.post(name: .BasicShippingInfo, infos: ["infos": BasicShippingInfo(price: shippingInfo.price, estimate: shippingInfo.estimate)])
        self.dismiss(animated: true, completion: nil)
    }
    
    func saveCategory(category: String) {
        self.post(name: .CategoryInfo, infos: ["infos": category])
        self.dismiss(animated: true, completion: nil)
    }
}
