//
//  InsightsViewController.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/24/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import UIKit

enum InsightsCell: Int {
    case monthlySale = 0
    case favoritedItems = 1
    case bestSellers = 2
    case gradesChart = 3
}

class InsightsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBAction func tapBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.addGradient()
        self.tableView.backgroundColor = .clear
        let monthtlySaleGraphCell = createCell(nibName: "BarCell")
        let favoritedItemsChartCell = createCell(nibName: "PieChartCell")
        let bestSellersChartCell = createCell(nibName: "PieChartCell")
        let gradesChartCell = createCell(nibName: "PieChartCell")
        
        tableView.register(monthtlySaleGraphCell, forCellReuseIdentifier: CellIds.barCell.rawValue)
        tableView.register(favoritedItemsChartCell, forCellReuseIdentifier: CellIds.piechartCell.rawValue)
        tableView.register(bestSellersChartCell, forCellReuseIdentifier: CellIds.piechartCell.rawValue)
        tableView.register(gradesChartCell, forCellReuseIdentifier: CellIds.piechartCell.rawValue)

        
        
        //store views daily --- compare it from last month
        
        //grade --> most checked views... best sellers ... most subscribers/followers(maybe)
        // Do any additional setup after loading the view.
        
        //reviews also
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InsightsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section: InsightsCell = InsightsCell(rawValue: indexPath.section) ?? .bestSellers

        switch section {
        case .monthlySale :
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIds.barCell.rawValue, for: indexPath) as! BarCell
            cell.displayCell(title: "Monthly Sale")
            return cell
        case .bestSellers :
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIds.piechartCell.rawValue, for: indexPath) as! PieChartCell
            cell.displayCell(title: "Best Sellers")
            return cell
        case .favoritedItems :
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIds.piechartCell.rawValue, for: indexPath) as! PieChartCell
            cell.displayCell(title: "Most Favorited")
            
            return cell
        case .gradesChart :
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIds.piechartCell.rawValue, for: indexPath) as! PieChartCell
            cell.displayCell(title: "Overall Grades")
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIds.piechartCell.rawValue, for: indexPath) as! PieChartCell
            cell.displayCell(title: "Default")
            return cell
        }
        
    }
    
    
    
}
