//
//  MyOrdersViewController.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/25/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import UIKit

class MyOrdersViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var listOforders = [testOrder]()
    
    
    @IBAction func tapBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func tapSegmentedControlAction(_ sender: Any) {
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        listOforders.append(testOrder(name: "Enzo Dosso", location: "6701 richfield Pkwy", status: "Completed", orderNumber: "1", date: "Monday february 14th 2021", paymentMode: "Le client a payé par carte banquaire"))
        listOforders.append(testOrder(name: "Franck Dosso", location: "6701 richfield Pkwy", status: "Pending", orderNumber: "2", date: "Monday february 14th 2021", paymentMode: "Le client a payé par carte banquaire"))
        listOforders.append(testOrder(name: "Paule Dosso", location: "6701 richfield Pkwy", status: "Pending", orderNumber: "3", date: "Monday february 14th 2021", paymentMode: "Le client a payé par carte banquaire"))
        listOforders.append(testOrder(name: "Marco", location: "6701 richfield Pkwy", status: "Processing", orderNumber: "5", date: "Monday february 14th 2021", paymentMode: "Le client a payé par carte banquaire"))
        listOforders.append(testOrder(name: "Exo", location: "6701 richfield Pkwy", status: "Cancelled", orderNumber: "4", date: "Monday february 14th 2021", paymentMode: "Le client a payé par carte banquaire"))
        listOforders.append(testOrder(name: "Lens", location: "6701 richfield Pkwy", status: "Processing", orderNumber: "6", date: "Monday february 14th 2021", paymentMode: "Le client a payé par carte banquaire"))
        listOforders.append(testOrder(name: "Vikay", location: "6701 richfield Pkwy", status: "Completed", orderNumber: "7", date: "Monday february 14th 2021", paymentMode: "Le client a payé par carte banquaire"))
        listOforders.append(testOrder(name: "Enz", location: "6701 richfield Pkwy", status: "Completed", orderNumber: "8", date: "Monday february 14th 2021", paymentMode: "Le client a payé par carte banquaire"))
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let cell = createCell(nibName: "OrderCell")
        tableView.register(cell, forCellReuseIdentifier: "orderCell")

        // Do any additional setup after loading the view.
    }
    
    
    func setupUI() {
        self.addGradient()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MyOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOforders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell", for: indexPath) as! OrderCell
        cell.displayCell(order: self.listOforders[indexPath.row])
        return cell
    }
    
    
    
}

struct testOrder {
    var name: String
    var location: String
    var status: String
    var orderNumber: String
    var date: String
    var paymentMode: String
}
