//
//  AddProductPresenter.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 3/30/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit

protocol AddProductView {
    func showImagePickerOptions(key: String)
    func retrieveItems()
    func showVariantTypes(variants: [String])
}


class AddProductPresenter {
    
    var view: AddProductView?
    let imageStore = ImageStore.shared
    var selectedVariant = ""
    var mainImage : UIImage?
    var firstImage: UIImage?
    var secondImage: UIImage?
    var thirdImage: UIImage?
    var description: String?
    var price: String?
    var stock: String?
    var selectedVariants: [String]?
    var shippingPrice: String?
    var estimatedShipping: String?
    var category: String?
    
    
    var tailleVariant = Variant(name: "Taille", variants: ["S","M","L"], placeHolder: "XL", stock: AppConstants.defaultNumber)
    var shadesVariant = Variant(name: "Shades", variants: ["Rose","Gold","Mate"], placeHolder: "Violet", stock: AppConstants.defaultNumber)
    var couleursVariant = Variant(name: "Couleurs", variants: ["Jaune","Vert","Rouge"], placeHolder: "Bleu", stock: AppConstants.defaultNumber)
    var materielsVariant = Variant(name: "Materiels", variants: ["Bois","Mauve","Loh"], placeHolder: "Shih", stock: AppConstants.defaultNumber)
    var cancelVariant = Variant(name: "Cancel", variants: ["Bois","Mauve","Loh"], placeHolder: "...", stock: AppConstants.defaultNumber)
    
    var allVariants = [Variant]()

    
    
    init(view: AddProductView) {
        self.view = view
        initializeVariants()
    }
    
    func start() {
        //let product = Product()
    }
    
    func initializeVariants() {
        allVariants.append(tailleVariant)
        allVariants.append(shadesVariant)
        allVariants.append(couleursVariant)
        allVariants.append(materielsVariant)
        allVariants.append(cancelVariant)
    }
    
    func saveShippingPriceAndEstimate(basicShippingInfo: BasicShippingInfo) {
        self.shippingPrice = basicShippingInfo.price
        self.estimatedShipping = basicShippingInfo.estimate
        self.view?.retrieveItems()
    }
    
    func saveCategory(category: String) {
        self.category = category
        self.view?.retrieveItems()
    }
    
    func saveStock(stock: String) {
        self.stock = stock
        self.view?.retrieveItems()
    }
    
    func savePrice(price: String) {
        self.price = price
        self.view?.retrieveItems()
    }
    
    func saveDescription(desc: String) {
        self.description = desc
        self.view?.retrieveItems()
    }
    
    func addVariant(variant: String) {
        
        var variantType = allVariants.first(where: {$0.name == selectedVariant})
        variantType?.addVariant(variant: variant)
        allVariants.removeAll(where: {$0.name == selectedVariant})
        if let variantType = variantType {
            allVariants.append(variantType)
            self.reloadView()
        }
    }

    
    func showImagePicker(key: String) {
        self.view?.showImagePickerOptions(key: key)
    }
    
    func updateVariantStock(stock: Int) {
        var variantType = allVariants.first(where: {$0.name == selectedVariant})
        variantType?.updateStock(stock: stock.description)
        allVariants.removeAll(where: {$0.name == selectedVariant})
        if let variantType = variantType {
            allVariants.append(variantType)
            self.reloadView()
        }
    }
    
    func getShippingInfo() -> String? {
        guard let shippingPrice = self.shippingPrice, let estimatedShipping = estimatedShipping else {
            return nil
        }
        return "Prix de livraison à \(shippingPrice)FCFA\n Livraison estimée pour \(estimatedShipping)"
    }
    
    func getCategorie() -> String? {
        guard let category = self.category else {
            return nil
        }
        return "Catégorie: \(category)"
    }
    
    func getImages() {
        mainImage = imageStore.getImage(key: AddimagesCell.image1Key)
        firstImage = imageStore.getImage(key: AddimagesCell.image2Key)
        secondImage = imageStore.getImage(key: AddimagesCell.image3Key)
        thirdImage = imageStore.getImage(key: AddimagesCell.image4Key)

    }
    
    func shouldButtonBeActive() -> Bool {
        getImages()
        return mainImage != nil && firstImage != nil && secondImage != nil && thirdImage != nil && description != nil && price != nil && stock != nil && shippingPrice != nil && estimatedShipping != nil && category != nil
    }
    
    
    func pullVariantTypes() {
        let variants = ["Taille","Shades","Couleurs","Materiels","Cancel"]
        self.view?.showVariantTypes(variants: variants)
    }
    
    func showVariants(variant: String) -> [String] {
        print(allVariants.first(where: {$0.name == variant})?.variants ?? [])
        
        return allVariants.first(where: {$0.name == variant})?.variants ?? []
    }
    
    func getPlaceHolderForVariant() -> String {
        return allVariants.first(where: {$0.name == selectedVariant})?.placeHolder ?? "..."
    }
    
    func reloadView() {
        view?.retrieveItems()
    }
}
