//
//  ManageBoutiquePresenter.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/25/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit

protocol ManageBoutiqueView {
    func start()
}
class ManageBoutiquePresenter  {
    
    var view: ManageBoutiqueView
    
    init(with view: ManageBoutiqueView) {
        self.view = view
    }
    
    
    func start() {
        
    }
    
}
