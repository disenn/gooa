//
//  AnyViewControllerPresenter.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 4/12/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation

struct BasicShippingInfo {
    var price: String
    var estimate: String
}
protocol AnyVCView {
    func saveShipping(shippingInfo: BasicShippingInfo)
    func saveCategory(category: String)
    func showPrompt(prompt: String)
    func dismissView()
}

class AnyViewControllerPresenter {
    
    var view: AnyVCView!
    var price: String?
    var estimate: String?
    var category: String?
    
    init(view: AnyVCView) {
        self.view = view
    }
    
    func saveEstimate(estimate: String) {
        self.estimate = estimate
    }
    
    func saveShippingPrice(price: String) {
        self.price = price
    }
    
    func savecategory(categories: [String]) {
        self.category = categories.first
    }
    
    func save(vc: SupportedVC) {
        switch vc {
        case .ShippingVC:
            guard let price = self.price else {
                self.view.showPrompt(prompt: "Faites entrer un prix")
                return
            }
            guard let estimate = self.estimate else {
                self.view.showPrompt(prompt: "Chosissez une estimation")
                return
            }
            let basicShippingInfo = BasicShippingInfo(price: price, estimate: estimate)
            self.view.saveShipping(shippingInfo: basicShippingInfo)
        case .CategoriesVC:
            guard let category = self.category else {
                self.view.showPrompt(prompt: "Choisissez une catégorie")
                return
            }
            self.view.saveCategory(category: category)
        default:
            return
        }
        self.view.dismissView()
    }
}
