//
//  ManageBoutiqueViewController.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/20/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import UIKit

class ManageBoutiqueViewController: UIViewController {
    
    @IBOutlet weak var menuButton: UIButton!
    
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var menuContainer: UIView!
    
    @IBOutlet weak var menuConstraint: NSLayoutConstraint!
    var constant: CGFloat = 0
    var menuVC: MenuViewController?
    var presenter: ManageBoutiquePresenter?
    
//    static func create() -> ManageBoutiqueViewController {
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "manageVC") as! ManageBoutiqueViewController
//        vc.presenter = ManageBoutiquePresenter(with: self)
//
//    }
     
    @IBAction func tapBack(_ sender: Any) {
        
    }
    
    var menuShow = false
    
    @IBOutlet weak var collectionViewTopConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    
//    var catImages = ["jeansCat", "dressCat","jeansCat", "dressCat","jeansCat", "dressCat"]
    var catImages = ["teesh", "teesh","teesh", "teesh","teesh", "teesh"]


    
    @IBAction func tapMenu(_ sender: UIButton) {
        menuShow = !menuShow
        if menuShow {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn) {
                self.menuConstraint.constant = (self.view.frame.width * 0.75) * (-1)
                self.view.layoutIfNeeded()
            } completion: { (_) in
                //
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn) {
                self.menuConstraint.constant = 0
                self.view.layoutIfNeeded()
            } completion: { (_) in
                //
            }
        }
        
    }
    
    var categoriesCount = 3

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        for child in self.children {
            menuVC = child as? MenuViewController
        }
        menuVC?.menuAction = self
        
    }

    
    
    func setupUI() {
        self.addGradient()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.categoryCollectionView.delegate = self
        self.categoryCollectionView.dataSource = self

        
        let itemCell = UINib(nibName: "ItemCell", bundle: nil)
        let itemCategoryCell = UINib(nibName: "ItemCategoryCell", bundle: nil)

        
        collectionView.register(itemCell, forCellWithReuseIdentifier: "itemCell")
        categoryCollectionView.register(itemCategoryCell, forCellWithReuseIdentifier: "itemcategoryCell")
        
        let layout = UICollectionViewFlowLayout()
        //layout.sectionInset = UIEdgeInsets(top: 10, left: 0, bottom: 20, right: 0)
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 40
        layout.itemSize = CGSize(width: (view.frame.width / 2) - 2, height: (view.frame.width / 2) - 2)
        collectionView.collectionViewLayout = layout
        menuButton.layer.borderWidth = 0.0
        menuButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        menuButton.layer.shadowRadius = 5.0
        menuButton.layer.shadowOpacity = 1.0
        menuButton.layer.shadowColor = UIColor.black.cgColor
        menuButton.roundButton()
        
        
        
        
    }

    
    
}

extension ManageBoutiqueViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //
        if collectionView == self.collectionView {
            return catImages.count
        }
        return categoriesCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //
        if collectionView == self.collectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: indexPath) as! Itemcell
            cell.layer.cornerRadius = 25.0
            cell.backgroundColor = .lightText
            cell.layer.borderWidth = 0.0
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.layer.shadowRadius = 5.0
            cell.layer.shadowOpacity = 0.5
            cell.layer.masksToBounds = false
            cell.displayCell(image: catImages[indexPath.row])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemcategoryCell", for: indexPath) as! ItemCategoryCell
            cell.layer.cornerRadius = 15.0
            cell.layer.borderWidth = 0.0
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 0)
            cell.layer.shadowRadius = 5.0
            cell.layer.shadowOpacity = 1
            cell.layer.masksToBounds = false
            cell.display(image: catImages[indexPath.row])
            return cell
        }
    }
    
    func animateCollectionView(value: CGFloat) {
        if value > 50 {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .transitionCurlUp) {
                self.collectionViewTopConstraint.constant = -124
                self.categoryCollectionView.alpha = 0.05
                self.view.layoutIfNeeded()
            } completion: { (_) in
            }
        } else {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .transitionFlipFromBottom) {
                self.collectionViewTopConstraint.constant = 22
                self.categoryCollectionView.alpha = 1.0
                self.view.layoutIfNeeded()
            } completion: { (_) in
            }
        }
        constant = value
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        animateCollectionView(value: scrollView.contentOffset.y)
    }
    
}

extension ManageBoutiqueViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionView {
            return CGSize(width: (collectionView.bounds.width / 2) - 5, height: (view.frame.height / 2.5))
        }
        return CGSize(width: (categoryCollectionView.bounds.width / CGFloat(categoriesCount)) - 5, height: categoryCollectionView.bounds.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
}

extension ManageBoutiqueViewController: MenuActions {
    func gotoOrders() {
        //
        self.present(createVC(id: "myordersVC", storyboard: .main), animated: true, completion: nil)
    }
    
    func gotoAbout() {
        //
        self.present(createVC(id: "aboutVC", storyboard: .main), animated: true, completion: nil)
    }
    
    func gotoSubscriptions() {
        //
    }
    
    func gotoCustomizeStore() {
        //
    }
    
    func gotoAddItems() {
        self.present(createVC(id: "addVC", storyboard: .main), animated: true, completion: nil)
    }
    
    func gotoStoreProfile() {
        //
    }
    
    func gotoInsights() {
        print("insights...")
        self.present(createVC(id: "insightsVC", storyboard: .main), animated: true, completion: nil)
    }
    
    func gotoTermsAndConditions() {
        //
    }
    
    func gotoQASession() {
        //
    }
    
    
}
