//
//  AboutViewController.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/27/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBAction func tapBackAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        self.addGradient()
        let cell = createCell(nibName: "DescriptionCell")
        tableView.backgroundColor = .clear
        tableView.register(cell, forCellReuseIdentifier: "descriptionCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.makeFooter()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AboutViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "descriptionCell", for: indexPath) as! DescriptionCell
        cell.displayCell(placeHolder: "", editable: true) { (words) in
            print("WORDS are \(words)")
        }
        //cell.displayCell(title: "A propos de vous", placeHolder: "Tell us about your business")
        return cell
    }
    
    
}
