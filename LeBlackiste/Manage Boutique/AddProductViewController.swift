//
//  AddProductViewController.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/18/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import UIKit

class AddProductViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    var listOfCells = [UINib]()
    var presenter: AddProductPresenter?
    var anyVCDelegate: AnyVCView?
    
    @IBAction func goBackAction(_ sender: Any) {
        self.post(name: .BasicShippingInfo, infos: ["infos": BasicShippingInfo(price: "10000", estimate: "24 heures")])
        self.dismiss(animated: true, completion: nil)
    }
    var variant = "Variant"
    var show = false
    var activeTextfieldYPosition = CGFloat(0)
    var activeIndexPath = IndexPath()
    let imageStore = ImageStore()
    


    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        presenter = AddProductPresenter(view: self)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        registerEvents()
        // Do any additional setup after loading the view.
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if activeTextfieldYPosition <= 0.0 {
            tableView.setContentOffset(CGPoint(x: 0, y: self.view.frame.height / 2.0), animated: true)
            return
        }
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            let scrollTo = activeTextfieldYPosition - keyboardHeight
            print("--\(activeTextfieldYPosition)")
            tableView.setContentOffset(CGPoint(x: 0, y: scrollTo), animated: true)
        }
    }
    
    func registerEvents() {
        self.register(name: .BasicShippingInfo, selector: #selector(getShippingInfos(_:)))
        self.register(name: .CategoryInfo, selector: #selector(getCategoryInfos(_:)))
    }
    
    @objc func getShippingInfos(_ notification: Notification) {
        if let data = notification.userInfo as? [String: BasicShippingInfo] {
                for shipping in data {
                    self.presenter?.saveShippingPriceAndEstimate(basicShippingInfo: shipping.value)
                }
            }
    }
    
    @objc func getCategoryInfos(_ notification: Notification) {
        if let data = notification.userInfo as? [String: String] {
                for category in data {
                    self.presenter?.saveCategory(category: category.value)
                }
            }
    }
    
    
    func setupUI() {
        addGradient()
        setupCells(firstTime: true)
    }
    
    
    func setupCells(firstTime: Bool) {
        let imagesCell = UINib(nibName: "AddimagesCell", bundle: nil)
        let descCell = UINib(nibName: "DescriptionCell", bundle: nil)
        let priceStockCell = UINib(nibName: "PriceStockCell", bundle: nil)
        let sizeCell = UINib(nibName: "SelectionCell", bundle: nil)
        let categoryCell = UINib(nibName: "SelectionCell", bundle: nil)
        let shippingCell = UINib(nibName: "SelectionCell", bundle: nil)
        let singleButtonCell = UINib(nibName: "SingleButton", bundle: nil)
        let variantCell = UINib(nibName: "VariantCell", bundle: nil)
        let addVariantCell = UINib(nibName: "AddVariantCell", bundle: nil)


        listOfCells.removeAll()
        listOfCells.append(imagesCell)
        listOfCells.append(descCell)
        listOfCells.append(priceStockCell)
        listOfCells.append(sizeCell)
        listOfCells.append(variantCell)
        listOfCells.append(categoryCell)
        listOfCells.append(shippingCell)
        listOfCells.append(singleButtonCell)
        if self.show {
            listOfCells.append(addVariantCell)
        } else {
            listOfCells.removeAll(where: {$0.description == addVariantCell.description})
        }

        if !firstTime {
            return
        }
        tableView.register(imagesCell, forCellReuseIdentifier: "addimagesCell")
        tableView.register(descCell, forCellReuseIdentifier: "descriptionCell")
        tableView.register(priceStockCell, forCellReuseIdentifier: "pricestockCell")
        tableView.register(sizeCell, forCellReuseIdentifier: "selectionCell")
        tableView.register(singleButtonCell, forCellReuseIdentifier: "singlebuttonCell")
        tableView.register(variantCell, forCellReuseIdentifier: VariantCell.getID)
        tableView.register(addVariantCell, forCellReuseIdentifier: AddVariantCell.getID)


        tableView.delegate = self
        tableView.dataSource = self
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddProductViewController: UITableViewDelegate, UITableViewDataSource, TextFieldActivity {
    func getCellIndexPath(indexPath: IndexPath) {
        self.activeIndexPath = indexPath
        let cell = tableView.cellForRow(at: indexPath)
        let ypos = cell?.frame.origin.y
        activeTextfieldYPosition = ypos ?? 0.0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 4 {
            if show {
                return presenter?.showVariants(variant: presenter?.selectedVariant ?? "").count ?? 0
            }
            return 0
        }
        return 1
//        return listOfCells.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return listOfCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "addimagesCell") as! AddimagesCell
            cell.display {
                self.presenter?.showImagePicker(key: AddimagesCell.image1Key)
            } action2: {
                self.presenter?.showImagePicker(key: AddimagesCell.image2Key)
            } action3: {
                self.presenter?.showImagePicker(key: AddimagesCell.image3Key)
            } action4: {
                self.presenter?.showImagePicker(key: AddimagesCell.image4Key)
            }

            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "descriptionCell") as! DescriptionCell
            cell.displayCell(placeHolder: "", editable: true) { (words) in
                print("WORDS are \(words)")
                self.presenter?.saveDescription(desc: words)
            }
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "pricestockCell") as! PriceStockCell
            cell.displayCell(indexPath) { (price) in
                //
                self.presenter?.savePrice(price: price)
            } actionStock: { (stock) in
                //
                self.presenter?.saveStock(stock: stock)
            }

            cell.delegate = self
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell") as! SelectionCell
            cell.displayCell(originalTitle: "Variant",title: self.presenter?.selectedVariant, action: ({ [weak self] in
                self?.presenter?.pullVariantTypes()
            }))
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: VariantCell.getID) as! VariantCell
            let variants = presenter?.showVariants(variant: presenter?.selectedVariant ?? "")
            cell.delegate = self
            cell.displayCell(indexPath, name: variants?[indexPath.row] ?? "") { (variantName) in
                //delete based on this name
                print(variantName)
            } getStockNumber: { (stock) in
                print("STCOK \(stock)")
            }
            return cell
        case 5:
            if self.show {
                let cell = tableView.dequeueReusableCell(withIdentifier: AddVariantCell.getID) as! AddVariantCell
                cell.displayCell(variantType: presenter?.selectedVariant.lowercased() ?? "", action: ({
                    self.popUpVariantTextfield()
                }))
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell") as! SelectionCell
                cell.displayCell(originalTitle: "Shipping",title: presenter?.getShippingInfo(), action: ({
                    let vc = AnyViewController.create(for: .ShippingVC)
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }))
                return cell
            }
            
        case 6:
            if show {
                let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell") as! SelectionCell
                cell.displayCell(originalTitle: "Shipping",title: presenter?.getShippingInfo(), action: ({
                    let vc = AnyViewController.create(for: .ShippingVC)
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }))
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell") as! SelectionCell
                cell.displayCell(originalTitle: "Catégorie", title: self.presenter?.getCategorie(), action: ({
                    let vc = AnyViewController.create(for: .CategoriesVC)
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }))
                return cell
            }
        case 7:
            if show {
                let cell = tableView.dequeueReusableCell(withIdentifier: "selectionCell") as! SelectionCell
                cell.displayCell(originalTitle: "Catégorie", title: self.presenter?.getCategorie(), action: ({
                    let vc = AnyViewController.create(for: .CategoriesVC)
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }))
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "singlebuttonCell", for: indexPath) as! SingleButton
               // cell.delegate = self
                cell.display(name: "Ajouter produit", active: self.presenter?.shouldButtonBeActive())
                return cell
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "singlebuttonCell", for: indexPath) as! SingleButton
           // cell.delegate = self
            cell.display(name: "Ajouter produit", active: self.presenter?.shouldButtonBeActive())
            return cell
        }
    }
    }

extension AddProductViewController: AddProductView {
    func showVariantTypes(variants: [String]) {
        let sheet = UIAlertController(title: "Choose one", message: "Les differents types de choix pour ce produit", preferredStyle: .actionSheet)
        for variant in variants {
            if variant == "Cancel" {
                let action = UIAlertAction(title: variant, style: .cancel) { (action) in
//                    self.show = false
//                    self.setupCells(firstTime: false)
                    self.presenter?.reloadView()
                }
                sheet.addAction(action)
            } else {
                let action = UIAlertAction(title: variant, style: .default) { (action) in
                    self.presenter?.selectedVariant = variant
                    self.show = true
                    self.setupCells(firstTime: false)
                    self.presenter?.reloadView()
                }
                sheet.addAction(action)
            }
        }
        self.present(sheet, animated: true, completion: nil)
    }
    
    func retrieveItems() {
        tableView.reloadData()
    }
    
    func popUpVariantTextfield() {
        let vc = UIAlertController(title: "", message: "Ajouter un/une \(variant)", preferredStyle: .alert)
        vc.addTextField { (textfield) in
            textfield.placeholder = self.presenter?.getPlaceHolderForVariant()
        }
        let action = UIAlertAction(title: "Ajouter", style: .default) { [weak self] (action) in
            if let variant = vc.textFields?.first?.text {
                self?.presenter?.addVariant(variant: variant)
            }
        }
        let cancelAction = UIAlertAction(title: "Annuler", style: .cancel, handler: nil)
        
        vc.addAction(action)
        vc.addAction(cancelAction)
        
        self.present(vc, animated: true, completion: nil)
    }
    
    func showImagePickerOptions(key: String) {
        let alertVC = UIAlertController(title: "Pick a Photo", message: "Choose a picture from Library or camera", preferredStyle: .actionSheet)
        alertVC.addGradient()
        
        //Image picker for camera
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { [weak self] (action) in
            //Capture self to avoid retain cycles
            guard let self = self else {
                return
            }
            
            let cameraImagePicker = self.imagePikerController(source: .camera, key: key)
            cameraImagePicker.delegate = self
            self.present(cameraImagePicker, animated: true) {
                //TODO
            }
        }
        
        //Image Picker for Library
        let libraryAction = UIAlertAction(title: "Library", style: .default) { [weak self] (action) in
            //Capture self to avoid retain cycles
            guard let self = self else {
                return
            }
            let libraryImagePicker = self.imagePikerController(source: .photoLibrary, key: key)
            libraryImagePicker.delegate = self
            self.present(libraryImagePicker, animated: true) {
                //TODO
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertVC.addAction(cameraAction)
        alertVC.addAction(libraryAction)
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
}
extension AddProductViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //
        let image = info[.originalImage] as! UIImage
        guard let title = picker.title else {
            return
        }
        imageStore.setImage(key: title, image: image)
        self.dismiss(animated: true) {
            self.presenter?.reloadView()
            //here we close loading view
        }
    }
    
}
