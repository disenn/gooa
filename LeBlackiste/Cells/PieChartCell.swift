//
//  PieChartCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/22/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit
import Charts

class PieChartCell: UITableViewCell {
    
    @IBOutlet weak var pieChartView: PieChartView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    func displayCell(title: String) {
        let players = ["CI jersey", "Nike hat", "Coque", "Auba"]
        let goals = [6, 8, 26, 30]
        titleLabel.text =  title
        customizeChart(dataPoints: players, values: goals.map{ Double($0) })

    }
    
    func customizeChart(dataPoints: [String], values: [Double]) {
      // TO-DO: customize the chart here
        // 1. Set ChartDataEntry
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
          let dataEntry = PieChartDataEntry(value: values[i], label: dataPoints[i], data: dataPoints[i] as AnyObject)
          dataEntries.append(dataEntry)
        }
        // 2. Set ChartDataSet
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: "Pie")
        pieChartDataSet.colors = colorsOfCharts(numbersOfColor: dataPoints.count)
        // 3. Set ChartData
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        pieChartData.setValueFormatter(formatter)
        // 4. Assign it to the chart’s data
        pieChartView.data = pieChartData
    }
    
    private func colorsOfCharts(numbersOfColor: Int) -> [UIColor] {
      var colors: [UIColor] = []
      for _ in 0..<numbersOfColor {
        let red = Double(arc4random_uniform(256))
        let green = Double(arc4random_uniform(256))
        let blue = Double(arc4random_uniform(256))
        let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
        colors.append(color)
      }
      return colors
    }
}

