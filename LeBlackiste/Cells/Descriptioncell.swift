//
//  Descriptioncell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/18/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit

class DescriptionCell: UITableViewCell {
    
    static var shared = DescriptionCell()
    
    var textviewAction: ((String) -> Void)?
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descTextview: UITextView!
    
    func displayCell(placeHolder: String, editable: Bool, action: @escaping ((String) -> Void)) {
        //titleLabel.text = title
        //descTextview.insertText(placeHolder)
        self.textviewAction = action
        descTextview.layer.borderWidth = 1
        descTextview.layer.borderColor = UIColor.black.cgColor
        descTextview.delegate = self
    }
}

extension DescriptionCell: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.backgroundColor = UIColor.lightGray
        if (textView.text == "Enter your notes here")
                {
                    textView.text = ""
                    textView.textColor = .black
                }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
            textView.backgroundColor = UIColor.white
            
            if (textView.text == "")
            {
                textView.text = "Enter your notes here"
                textView.textColor = .lightGray
            } else {
                guard let action = self.textviewAction else {
                    return
                }
                action(textView.text)
            }
        }
    
    func textViewDidChange(_ textView: UITextView) {
        //
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
