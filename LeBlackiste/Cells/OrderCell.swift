//
//  OrderCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/25/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit


class OrderCell: UITableViewCell {
    
    @IBOutlet weak var barStatusColor: UIView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    
    
    @IBOutlet weak var orderNumberLabel: UILabel!
    
    
    @IBOutlet weak var paymentModeLabel: UILabel!
    
    
    
    func displayCell(order: testOrder) {
        setupColor(status: order.status)
        nameLabel.text = order.name
        statusLabel.text = order.status
        dateLabel.text = order.date
        locationLabel.text = order.location
        orderNumberLabel.text = "order #\(order.orderNumber)"
        paymentModeLabel.text = order.paymentMode
    }
    
    func setupColor(status: String) {
        if status  == "Completed" {
            barStatusColor.backgroundColor = UIColor.green
            statusLabel.textColor = .green
            
        } else if status == "Processing" {
            barStatusColor.backgroundColor = .yellowTheme
            statusLabel.textColor = .yellowTheme
            
        } else if status == "Pending" {
            barStatusColor.backgroundColor = .lightGray
            statusLabel.textColor = .lightGray
        } else {
            barStatusColor.backgroundColor = .red
            statusLabel.textColor = .red
        }
    }
}
