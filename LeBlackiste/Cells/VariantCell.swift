//
//  VariantCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 3/30/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit

struct Variant {
    var name: String
    var variants = [String]()
    var placeHolder: String
    var stock: String
    
    mutating func addVariant(variant: String) {
        self.variants.append(variant)
    }
    
    mutating func updateStock(stock: String) {
        self.stock = stock
    }
}

protocol TextFieldActivity {
    func getCellIndexPath(indexPath: IndexPath)
}

class VariantCell: UITableViewCell {
    
    @IBOutlet weak var stockTextfield: UITextField!
    
    @IBOutlet weak var mainStackView: UIStackView!
    
    //static var shared = VariantCell()
    static var getID = "variantCell"
    @IBOutlet weak var nameLabel: UILabel!
    var deleteAction: ((String) -> Void)?
    var getStockNumber: ((String) -> Void)?
    var variant = ""
    var delegate: TextFieldActivity?
    var indexPath = IndexPath()
    
    @IBOutlet weak var mainStackViewBottomConstraint: NSLayoutConstraint!
    
    
    @IBAction func tapDeleteAction(_ sender: Any) {
        if let action = deleteAction {
            action(variant)
        }
    }
    
    
    func displayCell(_ indexPath: IndexPath, name: String, deleteAction: @escaping ((String) -> Void), getStockNumber: @escaping ((String) -> Void)) {
        nameLabel.text = name
        self.indexPath = indexPath
        stockTextfield.delegate = self
        self.variant = name
        self.deleteAction = deleteAction
        self.getStockNumber = getStockNumber
        //let tapGesture = UITapGestureRecognizer(target: self, action: #selector(checkCellPosition))
        //stockTextfield.addGestureRecognizer(tapGesture)
        stockTextfield.addTarget(self, action: #selector(checkCellPosition), for: .allEvents)

    }
    
    @objc func checkCellPosition() {
        delegate?.getCellIndexPath(indexPath: indexPath)
    }
}

extension VariantCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard textField.text?.count ?? 0 <= 4 else {
            if string == "" {
                return true
            }
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let action = getStockNumber {
            action(textField.text ?? "----")
        }
    }
    
}
