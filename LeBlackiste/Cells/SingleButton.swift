//
//  SingleButton.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 11/9/20.
//  Copyright © 2020 TechNoire. All rights reserved.
//

import Foundation
import UIKit

protocol SingleActionView {
    func action()
}
class SingleButton: UITableViewCell {
    
    static var shared = SingleButton()

    
    @IBOutlet weak var myButton: UIButton!
    var delegate: SingleActionView?
    
    @IBAction func touchupAction(_ sender: Any) {
        self.delegate?.action()
    }
    
    
    
    func display(name: String, active: Bool?) {
        myButton.layer.cornerRadius = myButton.frame.height / 2
        myButton.setTitle(name, for: .normal)
        
        myButton.backgroundColor = active ?? false ? .blackTheme : .lightGray
    }
}
