//
//  ItemCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/19/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit

class Itemcell: UICollectionViewCell {
    
    @IBOutlet weak var favoriteButton: UIButton!
    
    @IBOutlet weak var itemImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var categoryLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var warningLabel: UILabel!
    
            
    
    @IBAction func tapFavoriteButton(_ sender: Any) {
        
    }
    

    
    func displayCell(image: String) {
        itemImageView.layer.cornerRadius = 25.0
        itemImageView.layer.borderWidth = 1.5
        itemImageView.layer.borderColor = UIColor.yellowTheme.cgColor
        
        self.layoutIfNeeded()
    }
}

