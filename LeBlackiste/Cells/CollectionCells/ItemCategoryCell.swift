//
//  ItemCategoryCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/20/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit

class ItemCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryButton: UIButton!
    
    
    @IBAction func tapCategoryCell(_ sender: Any) {
        //
    }
    
    
    
    func display(image: String)  {
        //
        categoryButton.setImage(UIImage(named: image), for: .normal)
        categoryButton.roundButton()
    }
}
