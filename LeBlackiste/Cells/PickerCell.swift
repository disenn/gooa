//
//  PickerCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 11/9/20.
//  Copyright © 2020 TechNoire. All rights reserved.
//

import Foundation
import UIKit

class PickerCell: UITableViewCell {
    
    @IBOutlet weak var pickerView: UIPickerView!
    var items = [String]()
    @IBOutlet weak var nameLabel: UILabel!
    
    
    func display(title: String, items: [String]) {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(resignFirstResponder))
        self.contentView.addGestureRecognizer(tapGesture)
        self.items = items
        nameLabel.text = title
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
    }
}
extension PickerCell: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return items[row]
    }
}
