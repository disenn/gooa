//
//  CategoriesPickerCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 11/9/20.
//  Copyright © 2020 TechNoire. All rights reserved.
//

import Foundation
import UIKit

class CategoriesPickerCell: UITableViewCell {
    
    var picked = 0
    var categoryLimit = 3
    var chosenCategories = [String]()
    var categoriesAction: (([String]) -> Void)?
    
    static var getID = "categoriespickerCell"
    
    @IBAction func touchAction(_ sender: UIButton) {
        sender.setTitleColor(.black, for: .selected)
        sender.setTitleColor(.black, for: .normal)
        
        if picked >= categoryLimit {
            if sender.isSelected {
                sender.isSelected = false
                sender.backgroundColor = .lightGray
                sender.tintColor = .lightGray
                picked -= 1
                trackLabel.text = "\(picked.description)/\(categoryLimit.description)"
                return
            }
            sender.backgroundColor = .lightGray
            sender.tintColor = .lightGray
        } else {
            sender.isSelected = !sender.isSelected
            if sender.isSelected {
                self.addCategory(category: sender.currentTitle)
                sender.backgroundColor = UIColor.yellowTheme
                sender.tintColor = UIColor.yellowTheme
                picked += 1
            } else {
                self.removeCategory(category: sender.currentTitle)
                sender.backgroundColor = .lightGray
                sender.tintColor = .lightGray
                picked -= 1
            }
        }
        if let action = self.categoriesAction {
            action(chosenCategories)
        }
        trackLabel.text = "\(picked.description)/\(categoryLimit.description)"
    }
    
    func addCategory(category: String?) {
        guard let category = category else {
            return
        }
        self.chosenCategories.append(category)
    }
    
    func removeCategory(category: String?) {
        guard let category = category else {
            return
        }
        self.chosenCategories.removeAll(where: {$0 == category})
    }
    
    
    @IBOutlet weak var trackLabel: UILabel!
    
    @IBOutlet weak var vetementsLabel: UIButton!
    @IBOutlet weak var lingeriesLabel: UIButton!
    
    @IBOutlet weak var robeLabel: UIButton!
    
    @IBOutlet weak var shoesLabel: UIButton!
    
    @IBOutlet weak var accesoiresLabel: UIButton!
    
    @IBOutlet weak var shortsButton: UIButton!
    
    @IBOutlet weak var jeansButton: UIButton!
    
    @IBOutlet weak var makeupsButton: UIButton!
    
    @IBOutlet weak var otherButton: UIButton!
    
    func setupUI() {
        vetementsLabel.layer.cornerRadius = vetementsLabel.frame.height / 2
        lingeriesLabel.layer.cornerRadius = vetementsLabel.frame.height / 2
        robeLabel.layer.cornerRadius = vetementsLabel.frame.height / 2
        shoesLabel.layer.cornerRadius = vetementsLabel.frame.height / 2
        accesoiresLabel.layer.cornerRadius = vetementsLabel.frame.height / 2
        shortsButton.layer.cornerRadius = vetementsLabel.frame.height / 2
        jeansButton.layer.cornerRadius = vetementsLabel.frame.height / 2
        makeupsButton.layer.cornerRadius = vetementsLabel.frame.height / 2
        otherButton.layer.cornerRadius = vetementsLabel.frame.height / 2

    }
    
    func display(categoryLimit: Int, actionCategories: @escaping (([String]) -> Void)) {
        self.categoryLimit = categoryLimit
        self.categoriesAction = actionCategories
        trackLabel.text = "\(picked.description)/\(categoryLimit.description)"
        setupUI()
    }
}
