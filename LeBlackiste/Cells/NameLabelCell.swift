//
//  NameLabelCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 11/9/20.
//  Copyright © 2020 TechNoire. All rights reserved.
//

import Foundation
import UIKit

class NameLabelCell: UITableViewCell {
    
    @IBOutlet weak var nameTextfield: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    
    
    func display(name: String, keyboardType: UIKeyboardType) {
        nameLabel.text = name
        nameTextfield.keyboardType = keyboardType
        nameTextfield.themeBorderDisplay()
        
    }
}
