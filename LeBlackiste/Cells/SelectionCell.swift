//
//  SelectionCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/18/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit


class SelectionCell: UITableViewCell {
    
    
    @IBOutlet weak var actionButton: UIButton!
    
    static var shared = SelectionCell()
    @IBAction func tapButton(_ sender: Any) {
        if let action = self.action {
            action()
        }
    }
    
    @IBOutlet weak var bgView: UIView!
    
    
    @IBOutlet weak var titleLabel: UILabel!
    var action: (()-> Void)?
    
    func displayCell(originalTitle: String, title: String?, action: @escaping (()-> Void)) {
        
        titleLabel.text = title == nil || title == "" ? originalTitle : title
        self.action = action
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        contentView.addGestureRecognizer(tapRecognizer)
        
        let buttonImage = title == nil || title == "" ? UIImage(named: Images.forwardArrow) : UIImage(named: Images.checkmark)
        actionButton.setImage(buttonImage, for: .normal)
        titleLabel.font = title == nil || title == "" ? .systemFont(ofSize: 17.0) : .boldSystemFont(ofSize: 15.0)
    }
    
    @objc func tapAction() {
        if let action = self.action {
            action()
        }
    }
}
