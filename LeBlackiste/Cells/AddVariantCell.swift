//
//  AddVariantCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 3/30/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit

class AddVariantCell: UITableViewCell {
    
    static var getID = "addvariantCell"
    var action: (()-> Void)?
    
    @IBOutlet weak var titleLabel: UILabel!
    
    func displayCell(variantType: String, action: @escaping (()-> Void)) {
        self.action = action
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        contentView.addGestureRecognizer(tapRecognizer)
        titleLabel.text = "Ajouter un/une autre \(variantType)"
    }
    
    @objc func tapAction() {
        if let action = self.action {
            action()
        }
    }
}
