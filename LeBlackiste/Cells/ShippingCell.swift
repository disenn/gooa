//
//  ShippingCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 4/6/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit

class ShippingCell: UITableViewCell {
    
    static var getID = "shippingCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var priceTextfield: UITextField!
    
    
    @IBOutlet weak var pikerView: UIPickerView!
    
    @IBOutlet weak var saveButton: UIButton!
    
    var days = ["Choose an estimate","24 heures","1 à 3 jour(s)","1 à 7 jour(s)", "Une semaine", "deux semaines"]
    
    var priceAction: ((String)-> Void)?
    var estimateAction: ((String)-> Void)?

    
    @IBAction func saveAction(_ sender: Any) {
        
    }
    
    
    
    func displayCell(actionPrice: @escaping ((String)-> Void), actionEstimate: @escaping ((String)->Void)) {
        self.priceAction = actionPrice
        self.estimateAction = actionEstimate
        priceTextfield.themeBorderDisplay()
        priceTextfield.delegate = self
        pikerView.delegate = self
        pikerView.dataSource = self
    }
}
extension ShippingCell: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return days.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return days[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let action = estimateAction {
            action(days[row])
        }
    }
}

extension ShippingCell: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let action = priceAction {
            action(textField.text ?? "")
        }
    }
}
