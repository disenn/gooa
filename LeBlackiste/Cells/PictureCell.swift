//
//  PictureCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 11/9/20.
//  Copyright © 2020 TechNoire. All rights reserved.
//

import Foundation
import UIKit

protocol PictureView {
    func imagePicker()
}

class PictureCell: UITableViewCell {
    
    
    @IBOutlet weak var myImageView: UIImageView!
    var pictureView: PictureView?
    
    @IBOutlet weak var logoButton: UIButton!
    
    @IBAction func addPictureAction(_ sender: Any) {

        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            pictureView?.imagePicker()
        }
    }
    
    func display(image: UIImage?) {
        logoButton.layer.cornerRadius = logoButton.frame.height / 2
        if let image = image {
            self.myImageView.image = image
        }
    }
}

//extension PictureCell : UINavigationControllerDelegate, UIImagePickerControllerDelegate {
//    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
//        self.dismiss(animated: true, completion: { () -> Void in
//
//        })
//
//        imageView.image = image
//    }
//}
