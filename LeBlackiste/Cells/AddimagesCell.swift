//
//  AddimagesCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/18/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit

class AddimagesCell: UITableViewCell {
    
    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var leftImage: UIImageView!
    
    @IBOutlet weak var middleImage: UIImageView!
    
    @IBOutlet weak var rightImage: UIImageView!
    
    var image1Action: (()-> Void)?
    var image2Action: (()-> Void)?
    var image3Action: (()-> Void)?
    var image4Action: (()-> Void)?
    let imageStore = ImageStore.shared
    
    static let image1Key = "image1Key"
    static let image2Key = "image2Key"
    static let image3Key = "image3Key"
    static let image4Key = "image4Key"

    
    
    
    @IBAction func touchMainImageAction(_ sender: Any) {
        if let action = self.image1Action {
            action()
        }
    }
    
    @IBAction func touchLeftImageButton(_ sender: Any) {
        if let action = self.image2Action {
            action()
        }
    }
    
    @IBAction func touchMiddleImageButton(_ sender: Any) {
        if let action = self.image3Action {
            action()
        }
    }
    
    
    @IBAction func touchRightImageButton(_ sender: Any) {
        if let action = self.image4Action {
            action()
        }
    }

    func display(action1: @escaping (()-> Void), action2: @escaping (()-> Void), action3: @escaping (()-> Void), action4: @escaping (()-> Void)) {
        self.image1Action = action1
        self.image2Action = action2
        self.image3Action = action3
        self.image4Action = action4
        assignImages()
    }
    
    func assignImages() {
        
        mainImage.image = imageStore.getImage(key: AddimagesCell.image1Key) ?? UIImage(named: "cam")
        leftImage.image = imageStore.getImage(key: AddimagesCell.image2Key) ?? UIImage(named: "cam")
        middleImage.image = imageStore.getImage(key: AddimagesCell.image3Key) ?? UIImage(named: "cam")
        rightImage.image = imageStore.getImage(key: AddimagesCell.image4Key) ?? UIImage(named: "cam")
        self.layoutIfNeeded()
    }
}

