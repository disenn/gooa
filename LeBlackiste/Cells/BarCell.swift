//
//  BarCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/22/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit
import Charts


class BarCell: UITableViewCell {
    
    @IBOutlet weak var barChartView: BarChartView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    func displayCell(title: String) {
        let players = ["Ozil", "Ramsey", "Laca", "Auba", "Xhaka", "Torreira"]
        let goals = [6, 1, 26, 1, 8, 2]
        titleLabel.text = title
        
        populateChart(dataPoints: players, values: goals.map{ Double($0) })
        
    }
    
    private func populateChart(dataPoints: [String], values: [Double]) {
        var dataEntries: [BarChartDataEntry] = []
        for i in 0..<dataPoints.count {
          let dataEntry = BarChartDataEntry(x: Double(i), y: Double(values[i]))
          dataEntries.append(dataEntry)
        }
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "Bar Chart View")
        let chartData = BarChartData(dataSet: chartDataSet)
        barChartView.data = chartData
    }
}
