//
//  NameAndLogoCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/22/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit


protocol MenuView {
    func tapItem()
}
class NameAndLogoCell: UITableViewCell {
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    @IBOutlet weak var circularView: UIView!
    
    @IBOutlet weak var numberLabel: UILabel!
    var action: (()-> Void)?
    
    
    
    func displayCell(title: String, image: String, action: @escaping (() ->Void)) {
        circularView.layer.cornerRadius = circularView.frame.height / 2
        titleLabel.text = title
        logoImageView.image = UIImage(named: image)
        logoImageView.layer.cornerRadius = logoImageView.frame.height / 2
        displayNotification(title: title)
        self.action = action

        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        self.contentView.addGestureRecognizer(gesture)
    }
    
    
    @objc func tapAction() {
        print("Tapped....")
        if let action = self.action {
            print("action go!!")
            action()
        }
    }
    
    func displayNotification(title: String) {
        if title == "Orders" {
            circularView.isHidden = false
            numberLabel.text = "9+"
        } else {
            circularView.isHidden = true
        }
    }
}
