//
//  PriceStockCell.swift
//  LeBlackiste
//
//  Created by Franck Dosso on 2/18/21.
//  Copyright © 2021 TechNoire. All rights reserved.
//

import Foundation
import UIKit

class PriceStockCell: UITableViewCell {
    
    @IBOutlet weak var priceField: UITextField!
    
    @IBOutlet weak var stockField: UITextField!
    var delegate: TextFieldActivity?
    var indexPath = IndexPath()
    
    var priceAction: ((String)-> Void)?
    var stockAction: ((String)-> Void)?

    
    func displayCell(_ indexPath: IndexPath, actionPrice: @escaping ((String)-> Void), actionStock: @escaping ((String) -> Void)) {
        self.priceAction = actionPrice
        self.stockAction = actionStock
        priceField.layer.borderWidth = 2.0
        priceField.layer.borderColor = UIColor.blackTheme.cgColor
        
        stockField.layer.borderWidth = 2.0
        stockField.layer.borderColor = UIColor.blackTheme.cgColor
        
        self.indexPath = indexPath
        priceField.addTarget(self, action: #selector(checkCellPosition), for: .allEvents)
        stockField.addTarget(self, action: #selector(checkCellPosition), for: .allEvents)
        
        self.priceField.delegate = self
        self.stockField.delegate = self

    }
    
    @objc func checkCellPosition() {
        delegate?.getCellIndexPath(indexPath: indexPath)
    }
}

extension PriceStockCell: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == priceField {
            guard let action = priceAction else {
                return
            }
            action(textField.text ?? "")
        } else {
            guard let action = stockAction else {
                return
            }
            action(textField.text ?? "")
        }
    }
}
